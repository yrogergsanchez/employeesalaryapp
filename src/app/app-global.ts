
export class Global{
    public static getYears(): number[] {
        const arrays = [];
        const currentYear = new Date().getFullYear();
        let index = currentYear - 10;
        for (index; index <= currentYear; index++) {
          arrays.push(index);
        }
        return arrays;
      }
    
      public static getMonths(): number[] {
        const arrays: number[] = [];
        for (let index = 1; index <= 12; index++) {
          arrays.push(index);      
        }    
        return arrays;
      }
}