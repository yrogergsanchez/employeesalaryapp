import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeComponent } from './pages/employee/employee.component';
import { EmployeeFormComponent } from './pages/employee-form/employee-form.component';

const routes: Routes = [
  {
    path: "employee/form",
    component: EmployeeFormComponent
  },
  {
    path: "employee/list",
    component: EmployeeComponent
  },
  {
    path: "**",
    redirectTo: "employee/list"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
