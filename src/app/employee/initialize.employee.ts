import * as moment from 'moment';
import { Employee } from './models/employee.interface';
import { Payment } from './models/payment.interface';

export class Initialize{
    public static Employee:Employee={
        id: 0,        
        beginDate: new Date(),
        birthday: new Date(),
        beginDateString: "",
        birthdayString: "",  
        employeeCode: "",
        employeeName: "",
        employeeSurname: "",       
        identificationNumber:"",        
        office:"",
        position:"", 
        division: "",
        grade: 1,      
        year: new Date().getFullYear(),
        month: undefined,
        commission: 0.00,
        compensationBonus: 0.00,
        contributions: 0.00,
        baseSalary: 0.0,
        productionBonus:0.00,
        totalSalary: 0.00,
        payments: ""
    };    
    public static Payment:Payment = {
        id: 0,
        commission: 0.00,
        compensationBonus: 0.00,
        contributions: 0.00,
        baseSalary: 0.0,
        productionBonus:0.00,
        totalSalary: 0.00,
        year: 0.00,
        month: 0.00
    }
}