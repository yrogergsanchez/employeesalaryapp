import { Component, Input, OnInit, ɵɵsetComponentScope } from '@angular/core';
import { Employee } from '../../models/employee.interface';
import { Initialize } from '../../initialize.employee';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Payment } from '../../models/payment.interface';
import Swal from 'sweetalert2';
import { Global } from 'src/app/app-global';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  public model: Payment = Initialize.Payment;
  public years: number[] = [];
  public months: number[] = [];
  public currentYear:number = new Date().getFullYear();
  @Input() public details: Employee[] = [];

  paymentGroup = new FormGroup({
    year: new FormControl('', Validators.required),
    month: new FormControl('', Validators.required),
    baseSalary: new FormControl('', Validators.required),
    productionBonus: new FormControl('', Validators.required),
    compensationBonus: new FormControl('', Validators.required),
    commission: new FormControl('', Validators.required),
    contributions: new FormControl('', Validators.required),
  });

  constructor(private toastr: ToastrService) { }

  ngOnInit(): void {
    const months = Global.getMonths();
    this.months = months;
    this.years = Global.getYears();
    this.model.year = this.currentYear;
    this.model.month = months[0];
  }

  public handleAddEmployee(): void {

    const {year, month} = this.model;

    const isValid = this.model.baseSalary <= 0;
    if (isValid) {
      this.toastr.warning("No puede agregar un pago con el salario base en 0");
    } else {
      if (this.details.length > 0) {
        const filter = this.details.filter(d => d.year === year && d.month === month);
        if (filter.length > 0) {
          this.toastr.warning("No puede agregar un pago con el mismo año y mes");
          return;
        }
        else {
         this.addDetail();
          this.clearPayment();
          return;
        }
      }
      else {
        this.addDetail();
        this.clearPayment();
        return;
      }
    }
  }

  public handleDeletePayment(employee: Employee): void {
    Swal.fire({
      title: 'Eliminar Pago',
      text: '¿Estas seguro que deseas realizar esta acción?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        if (employee.id > 0) {

        }
        else {
          const index = this.details.indexOf(employee);
          if (index !== -1) {
            this.details.splice(index, 1);
          }
          this.toastr.success("Pago eliminado satisfactoriamente");
        }
        Swal.close();

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.close();
      }
    })
  }

  public addDetail(): void{
    const {
      id,
      baseSalary,
      commission,
      compensationBonus,
      contributions,
      month,
      productionBonus,      
      year
    } = this.model;    

    const employee = Initialize.Employee;
    employee.baseSalary = baseSalary;
    employee.commission = commission;
    employee.compensationBonus = compensationBonus;
    employee.contributions = contributions;
    employee.month = parseInt(month.toString().split("-")[0]);
    employee.productionBonus = productionBonus;
    employee.year = year;

    this.details.push(employee);
  }

  public clearPayment(): void {
    this.model.year = new Date().getFullYear();
    this.model.baseSalary = 0.00;
    this.model.commission = 0.00;
    this.model.compensationBonus = 0.00;
    this.model.contributions = 0.00;
    this.model.productionBonus = 0.00;
  }

}
