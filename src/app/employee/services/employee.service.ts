import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../models/employee.interface';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseUrl:string = environment.baseUrl;
  constructor(private http: HttpClient) {   
  }

  create(employee:Employee[]):Observable<any>{
    return this.http.post<any>(`${this.baseUrl}api/CreateEmployee/create`, employee);
  }

  update(id:number, employee:Employee[]):Observable<any>{
    return this.http.put<any>(`${this.baseUrl}api/UpdateEmployee/update/${id}`, employee);
  }

  getEmployeeAll(pagination:any):Observable<Employee[]>{
    return this.http.post<Employee[]>(`${this.baseUrl}api/GetEmployee/get-all`, pagination);
  }

  getEmployeeByIdentification(identification:string):Observable<Employee>{
    return this.http.get<Employee>(`${this.baseUrl}api/GetEmployeeByIdentification/${identification}`);
  }

  getEmployeeByGrade(model:any):Observable<Employee[]>{
    return this.http.post<Employee[]>(`${this.baseUrl}api/GetEmployeeByGrade/get-employees-by-grade`, model);
  }

  getEmployeeByPositionAndGrade(model:any):Observable<Employee[]>{
    return this.http.post<Employee[]>(`${this.baseUrl}api/GetEmployeeByPositionAndGrade/get-employees-by-position-and-grade`, model);
  }

  getEmployeeByOfficeAndGrade(grade:number, office: string):Observable<Employee[]>{
    return this.http.get<Employee[]>(`${this.baseUrl}api/GetEmployeeByOfficeAndGrade/${grade}/${office}`);
  }  
}
