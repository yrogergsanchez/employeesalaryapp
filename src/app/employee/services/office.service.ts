import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Office } from '../models/office.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OfficeService {

  private baseUrl:string = environment.baseUrl;
  constructor(private http: HttpClient) {   
  }

  getOffices():Observable<Office[]>{
    return this.http.get<Office[]>(`${this.baseUrl}api/getoffice/get-all`);
  }
}
