import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Position } from '../models/position.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PositionService {

  private baseUrl:string = environment.baseUrl;
  constructor(private http: HttpClient) {   
  }

  getPositions():Observable<Position[]>{
    return this.http.get<Position[]>(`${this.baseUrl}api/getposition/get-all`);
  }

}
