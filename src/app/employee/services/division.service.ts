import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Division } from '../models/division.interface';

@Injectable({
  providedIn: 'root'
})
export class DivisionService {

  private baseUrl:string = environment.baseUrl;
  constructor(private http: HttpClient) {   
  }

  getDivisions():Observable<Division[]>{
    return this.http.get<Division[]>(`${this.baseUrl}api/getdivision/get-all`);
  }

}
