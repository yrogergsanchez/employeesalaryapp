import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { EmployeeComponent } from './pages/employee/employee.component';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeFormComponent } from './pages/employee-form/employee-form.component';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { PaymentComponent } from './components/payment/payment.component';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    EmployeeComponent,
    EmployeeFormComponent,
    PaymentComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EmployeeRoutingModule,
    NgxMaskModule.forRoot(maskConfig),
    NgbModule
  ]
})
export class EmployeeModule { }
