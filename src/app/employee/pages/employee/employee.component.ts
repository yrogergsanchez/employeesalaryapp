import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../models/employee.interface';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  
  public employees:Employee[] = [];
  public searchText:string = "";
  public cargarMasDisable = true;
  public page:number = 1;

  constructor(private employeeService:EmployeeService) { }

  async ngOnInit(): Promise<void> {
    const pagination = {
        search: '',
        page: 1,
        length: 10
      };
    this.employees = await this.employeeService.getEmployeeAll(pagination).toPromise();   
    this.page++;  
  }

   getEmployeeByGrade(grade:number):void{ 
    this.page = 1;  
    const model = {
      grade,
      pagination : {
        search: '',
        page: this.page,
        length: 10
      }
    };
     this.employeeService.getEmployeeByGrade(model).subscribe(employees =>{
      this.employees = employees;
      this.cargarMasDisable = employees.length < 10 ? true : false;
      this.page++;  
    });     
        
  }

   getEmployeeByGradeAndOffice(grade:number, office:string):void{
     this.employeeService.getEmployeeByOfficeAndGrade(grade, office).subscribe(employees =>{
       this.employees = employees;
       this.cargarMasDisable = employees.length < 10 ? true : false;
       this.page++;  
     });     
  }

   getEmployeeByGradeAndPosition(grade:number, position:string):void{     
    this.page = 1;
    const model = {
      grade,
      position,
      pagination : {
        search: '',
        page: this.page,
        length: 10
      }
    };
     this.employeeService.getEmployeeByPositionAndGrade(model).subscribe(employees =>{
      this.employees = employees;
      this.cargarMasDisable = employees.length < 10 ? true : false;
      this.page++;    
    });     
  }

  public async search():Promise<void>{
    if (this.searchText.length > 0) {
      this.page = 1;
      const pagination = {
        search: this.searchText,
        page: this.page,
        length: 10
      };
      this.employees = await this.employeeService.getEmployeeAll(pagination).toPromise(); 
      this.cargarMasDisable = this.employees.length < 10 ? true : false;
      this.page++;    
    }      
  }

  public async loadMore():Promise<void>{    
      const pagination = {
        search: this.searchText,
        page: this.page,
        length: 10
      };
      const employees:Employee[] = await this.employeeService.getEmployeeAll(pagination).toPromise(); 
      employees.forEach(employee => {
        this.employees.push(employee);
      })
      this.cargarMasDisable = this.employees.length < 10 ? true : false;  
      this.page++;    
  }

}
