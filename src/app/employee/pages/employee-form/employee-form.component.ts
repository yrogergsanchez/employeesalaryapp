import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { Employee } from '../../models/employee.interface';
import { Initialize } from '../../initialize.employee';
import { Division } from '../../models/division.interface';
import { Office } from '../../models/office.interface';
import { Position } from '../../models/position.interface';
import { DivisionService } from '../../services/division.service';
import { OfficeService } from '../../services/office.service';
import { PositionService } from '../../services/position.service';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from '../../services/employee.service';
import * as moment from 'moment';
import {NgbCalendar, NgbDate, NgbDatepicker, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { window } from 'ngx-bootstrap/utils';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
  
  public model: Employee = Initialize.Employee;
  
  public details: Employee[] = [];
  public searchedIdentification: boolean = false;

  public divisions: Division[] = [];
  public offices: Office[] = [];
  public positions: Position[] = [];

  group = new FormGroup({
    office: new FormControl('', Validators.required),
    employeeCode: new FormControl('', Validators.required),
    employeeName: new FormControl('', Validators.required),
    employeeSurname: new FormControl('', Validators.required),
    division: new FormControl('', Validators.required),
    position: new FormControl('', Validators.required),
    beginDate: new FormControl('', Validators.required),
    birtday: new FormControl('', Validators.required),
    identificationNumber: new FormControl('', Validators.required)
  });

  constructor(private activatedRoute: ActivatedRoute,
    private divisionService: DivisionService,
    private officeService: OfficeService,
    private positionService: PositionService,
    private employeeService:EmployeeService,
    private toastr: ToastrService,
    private calendar: NgbCalendar,
    private router: Router) {
      this.activatedRoute.queryParams.subscribe(params => {
        this.model.identificationNumber = params.identification;            
      });
     }

  async ngOnInit(): Promise<void> {    
    this.divisions = await this.divisionService.getDivisions().toPromise();
    this.offices = await this.officeService.getOffices().toPromise();
    this.positions = await this.positionService.getPositions().toPromise();   
    
    if (this.model.identificationNumber.trim().length > 0) {
      const employee = await this.employeeService.getEmployeeByIdentification(this.model.identificationNumber).toPromise();
      this.searchedIdentification = true;
      this.disableControl(false);     
      this.details = JSON.parse(employee.payments);   
      this.model = employee;
      this.model.beginDateString = "01012020";
      this.model.birthdayString = "01011990";
    }else{
      this.disableControl();
      this.searchedIdentification = false;
      this.model.id = 0;
    }
    
     
  }
  public handleAddEmployee():void{
    console.log(this.model);
    this.model.baseSalary = this.model.baseSalary || 0;

    const isValid = this.model.baseSalary <= 0;
      
    if (isValid) {
      this.toastr.warning("No puede agregar un pago con el salario base en 0");
    }
    else{
      if (this.details.length > 0) {
        const filter = this.details.filter(d => d.year === this.model.year && d.month === this.model.month);
        if (filter.length > 0) {
          this.toastr.warning("No puede agregar un pago con el mismo anio y mes");
        }
        else{
          this.details.push(this.model);          
        }  
      }
      else{
        console.log(this.model);
        this.details.push(this.model);      
      }      
    }  
  }

  public async handleGetEmployeeByIdentification(): Promise<void> {
    if (this.hasLyrics(this.model.identificationNumber)) {
      this.toastr.warning("El numero de identificacion solo debe contener numeros");
      return;
    }

    if (this.model.identificationNumber.length === 10) {
      const employee = await this.employeeService.getEmployeeByIdentification(this.model.identificationNumber).toPromise();
      console.log(employee, "employee");
      if (employee === null) {
        this.searchedIdentification = true;
        this.disableControl(false);
      }   
      else{
        this.toastr.warning("Ya existe un empleado con este número de identificación");
        this.model.identificationNumber = "";
        return;
      }  
    }
    else {
      this.toastr.warning("El numero de identificacion no es valido, favor introducir 10 digitos");
    }
  }

  disableControl(isDisabled: boolean = true): void {
    if (isDisabled) {
      this.group.get('office')?.disable();
      this.group.get('employeeCode')?.disable();
      this.group.get('employeeName')?.disable();
      this.group.get('employeeSurname')?.disable();
      this.group.get('division')?.disable();
      this.group.get('position')?.disable();
      this.group.get('beginDate')?.disable();
      this.group.get('birtday')?.disable();     
    } else {
      this.group.get('office')?.enable();
      this.group.get('employeeCode')?.enable();
      this.group.get('employeeName')?.enable();
      this.group.get('employeeSurname')?.enable();
      this.group.get('division')?.enable();
      this.group.get('position')?.enable();
      this.group.get('beginDate')?.enable();
      this.group.get('birtday')?.enable();     
    }
  }

  public handleSave():void{
    let isValid = true;
    
    this.details.map((employee:Employee) => {
                
      if (moment(this.model.beginDateString).isValid()) {
        this.toastr.warning("La fecha de inicio es invalidad");
        isValid = false;
        return;
      }         
     
      if (moment(this.model.birthdayString).isValid()) {
        this.toastr.warning("La fecha de cumpleanios es invalidad");
        isValid = false;
        return;
      }
      
      employee.identificationNumber = this.model.identificationNumber;
      employee.employeeSurname = this.model.employeeSurname;
      employee.employeeName = this.model.employeeName;
      employee.beginDate = this.getFormattedDate(this.model.beginDateString);
      employee.birthday = this.getFormattedDate(this.model.birthdayString);
      employee.division = this.model.division;
      employee.grade = this.model.grade;
      employee.office = this.model.office;
      employee.position = this.model.position; 
      employee.employeeCode = this.model.employeeCode;
    });
    
    if (!isValid) {
      return;
    }
     
    if (this.model.id > 0) {
      this.employeeService.update(this.model.id, this.details)
      .subscribe(response => {        
        this.toastr.success("Empleado actualizado satisfactoriamente");  
        this.router.navigateByUrl('/employee/list');         
      }, 
        err => {
          console.log(err, "err");
          this.toastr.error("Hubo un error actualizando la informacion del empleado");
         
        });
    }else{
      this.employeeService.create(this.details)
      .subscribe(response => {        
        this.toastr.success("Empleado guardado satisfactoriamente");  
        this.router.navigateByUrl('/employee/list');  
      }, 
        err => {
          console.log(err, "err");
          this.toastr.error("Hubo un error guardando la informacion del empleado");
        });
    }    
  }

  public hasLyrics(letter: string): boolean {
    var regExp = /[a-zA-Z]/g;
    return regExp.test(letter);
  }

  public getFormattedDate(date:string):Date{
    const day = date.substr(0, 2);
    const month = date.substr(2, 2);;
    const year = date.substr(4, 4);;
    const dateString = `${day}-${month}-${year}`;
    return new Date(dateString)
  }

}
