export interface Payment{
    id:                  number;
    baseSalary:          number;
    productionBonus:     number;
    compensationBonus:   number;
    commission:          number;
    contributions:       number;
    totalSalary:         number;
    year:                number;
    month:               number;
}