export interface Employee {
    id:               number;
    year:             number;
    month?:            number;
    office:           string;
    employeeCode:     string;
    employeeName:     string;
    employeeSurname:  string;
    division:         string;
    position:         string;
    grade:            number;
    beginDate:        Date;
    beginDateString:  string;    
    birthday:         Date;
    birthdayString:   string;
    identificationNumber: string;
    baseSalary:          number;
    productionBonus:     number;
    compensationBonus:   number;
    commission:          number;
    contributions:       number;
    totalSalary:         number;
    payments:            string;
}